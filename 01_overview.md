# ACF ContentPress Manual

The ACFCP Plugins provide a way to interact with the ACF PRO plugin via PHP.

### Abbreviations

WP: WordPress (Blogging/CMS platform)
ACF: Advanced Custom Fields (WP plugin)
ACF PRO: Paid Pro version of ACF
ACFCP: ACF ContentPress (WP plugin)
ACFCPFE / ACFCP Frontend: ACF ContentPress Frontend (WP plugin)
ACFCP I18n: ACF ContentPress Internationalization (WP plugin)
CPT: Custom Post Type

## ACF ContentPress

Provides an interface to create ACF Field Groups in PHP. Additionally, you may register Custom Post Types and Taxonomies directly in the code.

## ACF ContentPress Frontend

Provides a `display` method to display ACF Field Groups, Fields and Flexible Content Fields.

## ACF ContentPress I18n

Makes your ACFCP-powered website multilanguage-ready. Configure the languages you need, and mark the fields you need translated. Set up translations for custom post type and taxonomy slugs, as well as taxonomy terms and menu items.

### Links

Every WordPress element with a permalink gets ACF Fields or other means to translate its slug. Permalink generation uses these slugs and a custom routing resolves these slugs back to posts.
Every ACF Field you select for translation gets duplicated for every language. ACFCP I18n then filters the output of ACFCP Frontend's `display` method to only return the active language.

### Home Page

If you call an url without a language specific link (exp. the home site: www.premiummedia.ch), you are automatically redirected to /{your-browser-http-accept-language}. You are then redirected to the home page you set in the WordPress settings under Settings > Reading > Front page displays (choose a static page). But now we know which language you want to get, so we can choose the appropriate version.

### Navigation

Also included is a custom navigation. Add items to the menu as you are used to, click save, ignore the warnings, reload the page and then choose language specific menu labels.
Then use the `acfcontentpressi18n\navigation\Navigation` class to get or display a navigation. For breadcrumbs use `acfcontentpressi18n\navigation\Breadcrumbs`
