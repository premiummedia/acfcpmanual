# ACF

ACF (Pro) is a WordPress plugin which allows you to create custom key/value storage fields on WordPress posts.

By default, every WordPress Post has a title and a content text field. For complex pages or custom post types however you may wish to use more and specific fields. (eg A custom post type `book` has a field `ISBN Number`)

## ACF Components

ACF provides the following components:

- Field Groups
- Fields
    - Repeater Fields
    - Flexible Fields
        - Layouts

All these components have specific settings which can be looked up in the official ACF documentation.

### ACF Components structure

- Field Groups
    - Field 0..n


    - RepeaterField
        - Field 0..n
    - Flexible Content Field
        - Layout 0..n
            - Field 0..n

The repeater and flexible layout fields are just there for reference. Of course they are regular fields and fit wherever a field fits (inside a field group, layout or repeater field).

Putting a flexible content field inside a repeater field inside a layout of a flexible content field inside a ...etc is probably a bad idea. Think about splitting stuff into multiple field groups and creating custom post types.

### Field Groups

The basic ACF building block is a field group.
Field groups are a container in which you may add fields.
Field groups also specify, via the `location` setting, where its fields are attached.

So you might create a `bookfieldgroup` with a field for an ISBN Number, and configure this field group to attach to posts of the type `book`.

### Fields

A field is a key/value storage. There are simple fields like the  `TextField` for storing strings, and complex fields like the `RelationShipField` for storing foreign keys or the `RepeaterField` and `FlexibleContentField`, which are used for repeating elements.

#### Repeater Fields

Repeater fields can have fields added to itself.

The perfect example is building an image slider.
You'd create a repeater field called `slider`. To this repeater field you can then add the image field `image` and the textarea field `caption`.
This would provide you with a structure in which you can add multiple "slides", consisting of an `image` and a `caption` field, to your slider.

#### Flexible Fields & Layouts

Whereas repeater fields are perfect for sliders, flexible layout fields are perfect for user-generated content where they can choose from pre-defined layouts, and add, delete and arrange them in any way they desire.
So instead of adding fields to a flexible content field, you add __layouts__. You can think of layouts as "mini field groups", because they can hold fields like a field group, but are always attached to a specific flexible content field.

A perfect example is a standard content page on any website. By creating a flexible content field, you can add a paragraph layout(title field and text field), an image layout(image field and alt text field), a gallery layout(with a repeater field), and so on. The user can then decide if and in which order they use the layouts.
