-   [ACF ContentPress Manual](#acf-contentpress-manual)
    -   [Abbreviations](#abbreviations)
    -   [ACF ContentPress](#acf-contentpress)
    -   [ACF ContentPress Frontend](#acf-contentpress-frontend)
    -   [ACF ContentPress I18n](#acf-contentpress-i18n)
        -   [Links](#links)
        -   [Home Page](#home-page)
        -   [Navigation](#navigation)
-   [ACF](#acf)
    -   [ACF Components](#acf-components)
        -   [ACF Components structure](#acf-components-structure)
        -   [Field Groups](#field-groups)
        -   [Fields](#fields)
            -   [Repeater Fields](#repeater-fields)
            -   [Flexible Fields & Layouts](#flexible-fields-layouts)
-   [ACF ContentPress](#acf-contentpress-1)
    -   [Features](#features)
    -   [Installation](#installation)
    -   [Usage](#usage)
        -   [Registering ACF Components, Custom Post Types and
            Taxonomies](#registering-acf-components-custom-post-types-and-taxonomies)
            -   [Custom Post Types](#custom-post-types)
            -   [Taxonomies](#taxonomies)
        -   [Autoloading](#autoloading)
        -   [Code Pointers](#code-pointers)
        -   [Replacing Standard WordPress
            Title](#replacing-standard-wordpress-title)
        -   [Field Collections](#field-collections)
        -   [Creating Field Groups &
            Fields](#creating-field-groups-fields)
            -   [Adding Repeater Fields](#adding-repeater-fields)
            -   [Adding Flexible Content
                Fields](#adding-flexible-content-fields)
                -   [Layouts](#layouts)
                -   [Flexible Content Field](#flexible-content-field)
        -   [Fields](#fields-1)
            -   [Bundled Fields](#bundled-fields)
                -   [CheckboxField](#checkboxfield)
                -   [DatepickerField](#datepickerfield)
                -   [DatetimepickerField](#datetimepickerfield)
                -   [EmailField](#emailfield)
                -   [FileField](#filefield)
                -   [Flexible ContentField](#flexible-contentfield)
                -   [GalleryField](#galleryfield)
                -   [GeolocationField](#geolocationfield)
                -   [ImageField](#imagefield)
                -   [NumberField](#numberfield)
                -   [OembedField](#oembedfield)
                -   [PageLinkField](#pagelinkfield)
                -   [PasswordField](#passwordfield)
                -   [PostObjectField](#postobjectfield)
                -   [RadioField](#radiofield)
                -   [RelationshipField](#relationshipfield)
                -   [RepeaterField](#repeaterfield)
                -   [SelectField](#selectfield)
                -   [TaxonomyField](#taxonomyfield)
                -   [TextareaField](#textareafield)
                -   [TextField](#textfield)
                -   [TrueFalseField](#truefalsefield)
                -   [UrlField](#urlfield)
                -   [UserField](#userfield)
                -   [WysiwygField](#wysiwygfield)
-   [ACF ContentPress Frontend](#acf-contentpress-frontend-1)
    -   [Features](#features-1)
    -   [Installation](#installation-1)
    -   [Usage](#usage-1)
        -   [Templates](#templates)
            -   [Naming Templates](#naming-templates)
                -   [Fieldgroups](#fieldgroups)
                -   [Fields](#fields-2)
                -   [Layouts](#layouts-1)
            -   [Template Context Data](#template-context-data)
                -   [ACF Data](#acf-data)
                -   [Post Id](#post-id)
                -   [Manipulating ACF Data Before Template
                    Rendering](#manipulating-acf-data-before-template-rendering)
-   [ACF ContentPress I18n](#acf-contentpress-i18n-1)
    -   [Features](#features-2)
    -   [Installation](#installation-2)
    -   [Usage](#usage-2)
        -   [Manage Languages](#manage-languages)
        -   [Mark Fields For Translation](#mark-fields-for-translation)
        -   [Translate Post & Page Slugs / Taxonomy & Terms / Navigation
            Labels](#translate-post-page-slugs-taxonomy-terms-navigation-labels)
            -   [Custom Post Type & Taxonomy
                Names](#custom-post-type-taxonomy-names)
        -   [Home Page Routing](#home-page-routing)

ACF ContentPress Manual
=======================

The ACFCP Plugins provide a way to interact with the ACF PRO plugin via
PHP.

### Abbreviations

WP: WordPress (Blogging/CMS platform) ACF: Advanced Custom Fields (WP
plugin) ACF PRO: Paid Pro version of ACF ACFCP: ACF ContentPress (WP
plugin) ACFCPFE / ACFCP Frontend: ACF ContentPress Frontend (WP plugin)
ACFCP I18n: ACF ContentPress Internationalization (WP plugin) CPT:
Custom Post Type

ACF ContentPress
----------------

Provides an interface to create ACF Field Groups in PHP. Additionally,
you may register Custom Post Types and Taxonomies directly in the code.

ACF ContentPress Frontend
-------------------------

Provides a `display` method to display ACF Field Groups, Fields and
Flexible Content Fields.

ACF ContentPress I18n
---------------------

Makes your ACFCP-powered website multilanguage-ready. Configure the
languages you need, and mark the fields you need translated. Set up
translations for custom post type and taxonomy slugs, as well as
taxonomy terms and menu items.

### Links

Every WordPress element with a permalink gets ACF Fields or other means
to translate its slug. Permalink generation uses these slugs and a
custom routing resolves these slugs back to posts. Every ACF Field you
select for translation gets duplicated for every language. ACFCP I18n
then filters the output of ACFCP Frontend's `display` method to only
return the active language.

### Home Page

If you call an url without a language specific link (exp. the home site:
www.premiummedia.ch), you are automatically redirected to
/{your-browser-http-accept-language}. You are then redirected to the
home page you set in the WordPress settings under Settings &gt; Reading
&gt; Front page displays (choose a static page). But now we know which
language you want to get, so we can choose the appropriate version.

### Navigation

Also included is a custom navigation. Add items to the menu as you are
used to, click save, ignore the warnings, reload the page and then
choose language specific menu labels. Then use the
`acfcontentpressi18n\navigation\Navigation` class to get or display a
navigation. For breadcrumbs use
`acfcontentpressi18n\navigation\Breadcrumbs`

ACF
===

ACF (Pro) is a WordPress plugin which allows you to create custom
key/value storage fields on WordPress posts.

By default, every WordPress Post has a title and a content text field.
For complex pages or custom post types however you may wish to use more
and specific fields. (eg A custom post type `book` has a field
`ISBN Number`)

ACF Components
--------------

ACF provides the following components:

-   Field Groups
-   Fields
    -   Repeater Fields
    -   Flexible Fields
        -   Layouts

All these components have specific settings which can be looked up in
the official ACF documentation.

### ACF Components structure

-   Field Groups
    -   Field 0..n

    -   RepeaterField
        -   Field 0..n
    -   Flexible Content Field
        -   Layout 0..n
            -   Field 0..n

The repeater and flexible layout fields are just there for reference. Of
course they are regular fields and fit wherever a field fits (inside a
field group, layout or repeater field).

Putting a flexible content field inside a repeater field inside a layout
of a flexible content field inside a ...etc is probably a bad idea.
Think about splitting stuff into multiple field groups and creating
custom post types.

### Field Groups

The basic ACF building block is a field group. Field groups are a
container in which you may add fields. Field groups also specify, via
the `location` setting, where its fields are attached.

So you might create a `bookfieldgroup` with a field for an ISBN Number,
and configure this field group to attach to posts of the type `book`.

### Fields

A field is a key/value storage. There are simple fields like the
`TextField` for storing strings, and complex fields like the
`RelationShipField` for storing foreign keys or the `RepeaterField` and
`FlexibleContentField`, which are used for repeating elements.

#### Repeater Fields

Repeater fields can have fields added to itself.

The perfect example is building an image slider. You'd create a repeater
field called `slider`. To this repeater field you can then add the image
field `image` and the textarea field `caption`. This would provide you
with a structure in which you can add multiple "slides", consisting of
an `image` and a `caption` field, to your slider.

#### Flexible Fields & Layouts

Whereas repeater fields are perfect for sliders, flexible layout fields
are perfect for user-generated content where they can choose from
pre-defined layouts, and add, delete and arrange them in any way they
desire. So instead of adding fields to a flexible content field, you add
**layouts**. You can think of layouts as "mini field groups", because
they can hold fields like a field group, but are always attached to a
specific flexible content field.

A perfect example is a standard content page on any website. By creating
a flexible content field, you can add a paragraph layout(title field and
text field), an image layout(image field and alt text field), a gallery
layout(with a repeater field), and so on. The user can then decide if
and in which order they use the layouts.

ACF ContentPress
================

ACF ContentPress is a WordPress plugin based on the ACF (Advanced Custom
Fields) WordPress plugin.

Features
--------

-   Create ACF Field Groups using PHP
-   Register Custom Post Types using PHP
-   Register Taxonomies using PHP

-   Easily create your own custom fields by extending
    acfcontentpress\\core\\Field
-   Replace the standard WordPress post title by defining your own title
    field

Installation
------------

Use Composer install: `composer require premiummedia/acfcontentpress`

Usage
-----

By default, ACFCP will look for a `contents` directory in in your active
theme folder. Inside this directory you may add folders to separate your
field group, field and layout definitions. Register your defined classes
in the `contents/register.php` file.

Go and have a look at the `example` folder in the acfcontentpress
repository.

### Registering ACF Components, Custom Post Types and Taxonomies

In the `contents/register.php` file, you register defined ACF field
groups by adding its lowercase classname as a string to the
\$fieldGroups array.

    class Contents{
        public static $fieldGroups = array(
            // ---
            'bookfieldgroup',
            ...
            // ---
        );

        public static $customPostTypes = array(
            'book' => array(
                'args' => array(
                    'label' => 'Buch',
                    'public' => true
                ),
                // 'names' for ACFCP I18n use
                /*'names' => array(
                    'de' => 'buch',
                    'fr' => 'livre',
                    'en' => 'book',
                    'it' => '?',
                )*/
            ),
            'anotherCPT' => array(
                ...
            )
        );

        public static $taxonomies = array(
            'tax1' => array(
                'object_type' => 'book',
                'args' => array(),
                // 'names' for ACFCP I18n use
                /*'names' => array(
                    'de' => 'tax1_de',
                    'fr' => 'tax1_fr',
                    'en' => 'tax1_en',
                    'it' => 'tax1_it'
                )*/
            ),
            'tax2' => array(
                ...
            )
        );
    }

#### Custom Post Types

Create custom post types by adding them to the \$customPostTypes
array(). The parameter syntax is the same as the standard WordPress
`register_post_type` function. Add the cpt name as array key and an
array() with an `args` key as value.

#### Taxonomies

Create taxonomies by adding them to the \$taxonomies array(). The
parameter syntax is the same as the standard WordPress
`register_taxonomy` function. Add the taxonomy name as array key and an
array() with an `object_type` and `args` key as value.

### Autoloading

While you need to `use` ACF ContentPress classes, field groups, fields
and layouts you create within `contents/[fieldgroups|fields|layouts]`
are autoloaded in the global namespace.

### Code Pointers

Field Groups, Fields and Layouts all share the same base class and the
same constructor, which is always
`($key, $label = '', $settings = array())`

The key is an identifying slug, which must be unique within an field
collection. More on field collections later. The label is a human
readable version of the key. If left blank, an uppercased version of the
key is used. The settings array is an array of settings for this
specific ACF component. Different fields, layouts and groups have
different settings. Have a look at the class you're using or extending
to see possible settings.

### Replacing Standard WordPress Title

Add array('replacesTitle' =&gt; true) to a (preferrably) TextField's
settings to use this field's value to replace the WordPress post\_title.

### Field Collections

Field collections are basically an array of field objects. For objects
which have a field collection, you can add fields by calling
`addField(Field $field, bool $translate)`. Components with a field
collection: - Field groups - Layouts - Repeater fields

-   Flexible content field

The flexible content field is a special case, because instead of adding
fields to it via `addField()`, you use the same function to add
pre-defined layouts: `addField(Layout $layout)`

### Creating Field Groups & Fields

Field groups can be defined by creating a class which extends
`acfcontentpress\core\FieldGroup`. Call the parent constructor and add
fields within the class constructor:

    use acfcontentpress\core\FieldGroup;
    use acfcontentpress\contrib\fields\TextField;

    class BookFieldGroup extends FieldGroup{

        public function __construct(){

            $key = 'bookfieldgroup';
            $label = 'Book';
            $settings = array(
                'location' => array(
                    array(
                        array(
                            'param' => 'post_type',
                            'operator' => '==',
                            'value' => 'book'
                        )
                    )
                )
            )

            parent::__construct($key, $label, $settings);

            $this->addField(
                new TextField(
                    'isbn',
                    'ISBN Number',
                    array(
                        'required' => true,
                        'maxlength' => 13
                    )
                )
            );
        }

    }

#### Adding Repeater Fields

Within field group or layout constructor:

    $repeater = $this->addField(
        new RepeaterField(
            'slider',
            'Slider',
            array(
                'button_label' => 'New Slide',
                'min' => 2,
                'max' => 10
            )
        )
    );

    $repeater->addField(
        new ImageField(
            'image',
            'Image',
            array(
                'required' => true
            )
        )
    );

    $repeater->addField(
        new TextareaField(
            'caption'
        )
    );

When calling `$this->addField`, the added field is returned. You can use
that to add fields to it by saving it in a variable and calling
`addField` on it.

#### Adding Flexible Content Fields

Flexible content fields have layouts added to them, which themselves
have a field collection. To get started, we first have to create one or
more layouts.

##### Layouts

Layouts are defined by extending `acfcontentpress\core\Layout` and look
pretty much the same as a field group. Note that there is no need for a
location setting, because we will be adding the layout to a flexible
field directly by calling
`$flexibeContentField->addField(Layout $layout)`. This allows you to
easily use your layout in multiple flexible content fields at the same
time.

    use acfcontentpress\core\Layout;
    use acfcontentpress\contrib\fields\TextField;
    use acfcontentpress\contrib\fields\TextareaField;

    class ParagraphLayout extends Layout{
        function __construct(){
            parent::__construct('paragraphlayout', 'Paragraph');

            $this->addField(
                new TextField('title')
            );

            $this->addField(
                new TextareaField('text', 'Text')
            );
        }
    }

##### Flexible Content Field

Note: *Using flexible content fields within layouts/flexible content
fields is theoretically possible but you might want to look for an
easier solution using multiple field groups and custom post types.*

Within a field group constructor:

    $fc = $this->addField(
        new FlexibleContentField(
            'multicontent',
            'Multicontent',
            array(
                'button_label' => 'Add Content'
            )
        )
    );

    $fc->addField(new ParagraphLayout());

### Fields

Fields are a basic key/value storage. You may create your own fields by
extending `acfcontentpress\core\Field`.

But most of the time, using the bundled fields in
`acfcontentpress\contrib\fields` will suffice.

Note the default settings, which, in addition to specific field
settings, all fields share:

    protected $globalSettings = array(
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => ''
        ),
        'default_value' => '',
        'acf_type' => 'field',
        'replacesTitle' => 0
    );

#### Bundled Fields

Here you will find a short overview, including the field-specific
settings.

##### CheckboxField

A basic checkbox.

    protected $defaultSettings = array(
        /* (array) Array of choices where the key ('red') is used as value and the value ('Red') is used as label */
        'choices' => array(
        ),
        /* (string) Specify the layout of the checkbox inputs. Defaults to 'vertical'.
        Choices of 'vertical' or 'horizontal' */
        'layout' => 0
    );

##### DatepickerField

A date picker.

    protected $defaultSettings = array(
        'display_format' => 'd.m.Y',
        'return_format' => 'd.m.Y',
        'first_day' => 1,
    );

##### DatetimepickerField

A date and time picker.

    protected $defaultSettings = array(
        'display_format' => 'd.m.Y H:i',
        'return_format' => 'd.m.Y H:i',
        'first_day' => 1,
        );

##### EmailField

A text field which includes email validation.

    protected $defaultSettings = array(
        /* (string) Appears within the input. Defaults to '' */
        'placeholder' => '',
        /* (string) Appears before the input. Defaults to '' */
        'prepend' => '',
        /* (string) Appears after the input. Defaults to '' */
        'append' => ''
    );

##### FileField

A file upload field.

    protected $defaultSettings = array(
        /* (string) Specify the type of value returned by get_field(). Defaults to 'array'.
        Choices of 'array' (File Array), 'url' (File URL) or 'id' (File ID) */
        'return_format' => 'array',
        /* (string) Specify the file size shown when editing. Defaults to 'thumbnail'. */
        'preview_size' => 'thumbnail',
        /* (string) Restrict the file library. Defaults to 'all'.
        Choices of 'all' (All Files) or 'uploadedTo' (Uploaded to post) */
        'library' => 'all',
        /* (int) Specify the minimum filesize in MB required when uploading. Defaults to 0
        The unit may also be included. eg. '256KB' */
        'min_size' => 0,
        /* (int) Specify the maximum filesize in MB in px allowed when uploading. Defaults to 0
        The unit may also be included. eg. '256KB' */
        'max_size' => 0,
        /* (string) Comma separated list of file type extensions allowed when uploading. Defaults to '' */
        'mime_types' => ''
    );

##### Flexible ContentField

The flexible content field acts as a blank canvas to which you can add
an unlimited number of layouts with full control over the order. Each
layout can contain 1 or more sub fields allowing you to create simple or
complex flexible content layouts.

    protected $defaultSettings = array(
        'button_label' => 'Add Row',
        'min' => '',
        'max' => '',
    );

##### GalleryField

    protected $defaultSettings = array(
        /* (int) Specify the minimum attachments required to be selected. Defaults to 0 */
        'min' => 0,
        /* (int) Specify the maximum attachments allowed to be selected. Defaults to 0 */
        'max' => 0,
        /* (string) Specify the image size shown when editing. Defaults to 'thumbnail'. */
        'preview_size' => 'thumbnail',
        /* (string) Restrict the image library. Defaults to 'all'.
        Choices of 'all' (All Images) or 'uploadedTo' (Uploaded to post) */
        'library' => 'all',
        /* (int) Specify the minimum width in px required when uploading. Defaults to 0 */
        'min_width' => 0,
        /* (int) Specify the minimum height in px required when uploading. Defaults to 0 */
        'min_height' => 0,
        /* (int) Specify the minimum filesize in MB required when uploading. Defaults to 0
        The unit may also be included. eg. '256KB' */
        'min_size' => 0,
        /* (int) Specify the maximum width in px allowed when uploading. Defaults to 0 */
        'max_width' => 0,
        /* (int) Specify the maximum height in px allowed when uploading. Defaults to 0 */
        'max_height' => 0,
        /* (int) Specify the maximum filesize in MB in px allowed when uploading. Defaults to 0
        The unit may also be included. eg. '256KB' */
        'max_size' => 0,
        /* (string) Comma separated list of file type extensions allowed when uploading. Defaults to '' */
        'mime_types' => ''
    );

##### GeolocationField

Note: *Add your Google API key in the WP ACFCP settings.*

    protected $defaultSettings = array(
        'center_lat' => '46.942975',
        'center_lng' => '7.445129',
        'zoom' => '12'
    );

##### ImageField

    protected $defaultSettings = array(
        /* (string) Specify the type of value returned by get_field(). Defaults to 'array'.
        Choices of 'array' (Image Array), 'url' (Image URL) or 'id' (Image ID) */
        'return_format' => 'array',
        /* (string) Specify the image size shown when editing. Defaults to 'thumbnail'. */
        'preview_size' => 'thumbnail',
        /* (string) Restrict the image library. Defaults to 'all'.
        Choices of 'all' (All Images) or 'uploadedTo' (Uploaded to post) */
        'library' => 'all',
        /* (int) Specify the minimum width in px required when uploading. Defaults to 0 */
        'min_width' => 0,
        /* (int) Specify the minimum height in px required when uploading. Defaults to 0 */
        'min_height' => 0,
        /* (int) Specify the minimum filesize in MB required when uploading. Defaults to 0
        The unit may also be included. eg. '256KB' */
        'min_size' => 0,
        /* (int) Specify the maximum width in px allowed when uploading. Defaults to 0 */
        'max_width' => 0,
        /* (int) Specify the maximum height in px allowed when uploading. Defaults to 0 */
        'max_height' => 0,
        /* (int) Specify the maximum filesize in MB in px allowed when uploading. Defaults to 0
        The unit may also be included. eg. '256KB' */
        'max_size' => 0,
        /* (string) Comma separated list of file type extensions allowed when uploading. Defaults to '' */
        'mime_types' => ''
    );

##### NumberField

    protected $defaultSettings = array(
        /* (string) Appears within the input. Defaults to '' */
        'placeholder' => '',
        /* (string) Appears before the input. Defaults to '' */
        'prepend' => '',
        /* (string) Appears after the input. Defaults to '' */
        'append' => '',
        /* (int) Minimum number value. Defaults to '' */
        'min' => '',
        /* (int) Maximum number value. Defaults to '' */
        'max' => '',
        /* (int) Step size increments. Defaults to '' */
        'step' => ''
    );

##### OembedField

    protected $defaultSettings = array(
        /* (int) Specify the width of the oEmbed element. Can be overridden by CSS */
        'width' => '',
        /* (int) Specify the height of the oEmbed element. Can be overridden by CSS */
        'height' => ''
    );

##### PageLinkField

    protected $defaultSettings = array(
        /* (mixed) Specify an array of post types to filter the available choices. Defaults to '' */
        'post_type' => '',
        /* (mixed) Specify an array of taxonomies to filter the available choices. Defaults to '' */
        'taxonomy' => '',
        /* (bool) Allow a null (blank) value to be selected. Defaults to 0 */
        'allow_null' => 0,
        /* (bool) Allow mulitple choices to be selected. Defaults to 0 */
        'multiple' => 0
    );

##### PasswordField

    protected $defaultSettings = array(
        /* (string) Appears within the input. Defaults to '' */
        'placeholder' => '',
        /* (string) Appears before the input. Defaults to '' */
        'prepend' => '',
        /* (string) Appears after the input. Defaults to '' */
        'append' => ''
    );

##### PostObjectField

    protected $defaultSettings = array(
        /* (mixed) Specify an array of post types to filter the available choices. Defaults to '' */
        'post_type' => '',
        /* (mixed) Specify an array of taxonomies to filter the available choices. Defaults to '' */
        'taxonomy' => '',
        /* (bool) Allow a null (blank) value to be selected. Defaults to 0 */
        'allow_null' => 0,
        /* (bool) Allow mulitple choices to be selected. Defaults to 0 */
        'multiple' => 0,
        /* (string) Specify the type of value returned by get_field(). Defaults to 'object'.
        Choices of 'object' (Post object) or 'id' (Post ID) */
        'return_format' => 'object'
    );

##### RadioField

    protected $defaultSettings = array(
        /* (array) Array of choices where the key ('red') is used as value and the value ('Red') is used as label */
        'choices' => array(
        ),
        /* (bool) Allow a custom choice to be entered via a text input */
        'other_choice' => 0,
        /* (bool) Allow the custom value to be added to this field's choices. Defaults to 0.
        Will not work with PHP registered fields, only DB fields */
        'save_other_choice' => 0,
        /* (string) Specify the layout of the checkbox inputs. Defaults to 'vertical'.
        Choices of 'vertical' or 'horizontal' */
        'layout' => 0
    );

##### RelationshipField

    protected $defaultSettings = array(
        /* (mixed) Specify an array of post types to filter the available choices. Defaults to '' */
        'post_type' => '',
        /* (mixed) Specify an array of taxonomies to filter the available choices. Defaults to '' */
        'taxonomy' => '',
        /* (array) Specify the available filters used to search for posts.
        Choices of 'search' (Search input), 'post_type' (Post type select) and 'taxonomy' (Taxonomy select) */
        'filters' => array('search', 'post_type', 'taxonomy'),
        /* (array) Specify the visual elements for each post.
        Choices of 'featured_image' (Featured image icon) */
        'elements' => array(),
        /* (int) Specify the minimum posts required to be selected. Defaults to 0 */
        'min' => 0,
        /* (int) Specify the maximum posts allowed to be selected. Defaults to 0 */
        'max' => 0,
        /* (string) Specify the type of value returned by get_field(). Defaults to 'object'.
        Choices of 'object' (Post object) or 'id' (Post ID) */
        'return_format' => 'object'
    );

##### RepeaterField

    protected $defaultSettings = array(
        'button_label' => 'Add Row',
        'min' => '',
        'max' => '',
        'layout' => 'row',
        'collapsed' => ''
    );

##### SelectField

    protected $defaultSettings = array(
        /* (array) Array of choices where the key ('red') is used as value and the value ('Red') is used as label */
        'choices' => array(
        ),
        /* (bool) Allow a null (blank) value to be selected. Defaults to 0 */
        'allow_null' => 0,
        /* (bool) Allow mulitple choices to be selected. Defaults to 0 */
        'multiple' => 0,
        /* (bool) Use the select2 interfacte. Defaults to 0 */
        'ui' => 0,
        /* (bool) Load choices via AJAX. The ui setting must also be true for this to work. Defaults to 0 */
        'ajax' => 0,
        /* (string) Appears within the select2 input. Defaults to '' */
        'placeholder' => ''
    );

##### TaxonomyField

    protected $defaultSettings = array(
        /* (string) Specify the taxonomy to select terms from. Defaults to 'category' */
        'taxonomy' => '',
        /* (array) Specify the appearance of the taxonomy field. Defaults to 'checkbox'
        Choices of 'checkbox' (Checkbox inputs), 'multi_select' (Select field - multiple), 'radio' (Radio inputs) or 'select' (Select field) */
        'field_type' => 'checkbox',
        /* (bool) Allow a null (blank) value to be selected. Defaults to 0 */
        'allow_null' => 0,
        /* (bool) Allow selected terms to be saved as relatinoships to the post */
        'load_save_terms'   => 0,
        /* (string) Specify the type of value returned by get_field(). Defaults to 'id'.
        Choices of 'object' (Term object) or 'id' (Term ID) */
        'return_format'     => 'id',
        /* (bool) Allow new terms to be added via a popup window */
        'add_term'          => 1
    );

##### TextareaField

    protected $defaultSettings = array(
        /* (string) Appears within the input. Defaults to '' */
        'placeholder' => '',
        /* (string) Restricts the character limit. Defaults to '' */
        'maxlength' => '',
        /* (int) Restricts the number of rows and height. Defaults to '' */
        'rows' => '',
        /* (new_lines) Decides how to render new lines. Detauls to 'wpautop'.
        Choices of 'wpautop' (Automatically add paragraphs), 'br' (Automatically add <br>) or '' (No Formatting) */
        'new_lines' => '',
        /* (bool) Makes the input readonly. Defaults to 0 */
        'readonly' => 0,
        /* (bool) Makes the input disabled. Defaults to 0 */
        'disabled' => 0
    );

##### TextField

    protected $defaultSettings = array(
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'maxlength' => '',
        'readonly' => 0,
        'disabled' => 0
    );

##### TrueFalseField

    protected $defaultSettings = array(
        /* (string) Text shown along side the checkbox */
        'message' => 0
    );

##### UrlField

    protected $defaultSettings = array(
        /* (string) Appears within the input. Defaults to '' */
        'placeholder' => ''
    );

##### UserField

    protected $defaultSettings = array(
        /* (array) Array of roles to limit the users available for selection */
        'role' => array(),
        /* (bool) Allow a null (blank) value to be selected. Defaults to 0 */
        'allow_null' => 0,
        /* (bool) Allow mulitple choices to be selected. Defaults to 0 */
        'multiple' => 0
    );

##### WysiwygField

    protected $defaultSettings = array(
        /* (string) Specify which tabs are available. Defaults to 'all'.
        Choices of 'all' (Visual & Text), 'visual' (Visual Only) or text (Text Only) */
        'tabs' => 'all',
        /* (string) Specify the editor's toolbar. Defaults to 'full'.
        Choices of 'full' (Full), 'basic' (Basic) or a custom toolbar (https://www.advancedcustomfields.com/resources/customize-the-wysiwyg-toolbars/) */
        'toolbar' => 'full',
        /* (bool) Show the media upload button. Defaults to 1 */
        'media_upload' => 1
    );

ACF ContentPress Frontend
=========================

AcF ContentPress Frontend is a WordPress plugin which provides an easy
way to render ACF field groups and fields using templates.

NOTE:*This does not disturb the WordPress post/page template selection.
It simply provides a `display()` method, which you can use wherever you
like.*

Features
--------

-   display ACF components using `display()`
-   split your code into templates specific to an ACF component
-   process data of an ACF component before rendering
-   get the WordPress post id in your templates using `$this->id`

Installation
------------

The ACF ContentPress plugin has to be installed and activated in order
for ACFCP Frontend to work.

Use Composer install:
`composer require premiummedia/acfcontentpressfrontend`

Usage
-----

At the heart of ACFCP Frontend ist the `display` function. It loads the
data associated with an ACF Component and renders a template using this
data.

### Templates

By default ACFCP Frontend will look for a `templates` directory in your
active theme folder. Inside this directory you may add folders to
seperate your field group, field and layout templates.

Basic folder structure:

-   templates
    -   fieldgroups
        -   ...
    -   fields
        -   ...
    -   layouts
        -   ...

#### Naming Templates

##### Fieldgroups

Create templates for field groups in the `templates/fieldgroups` folder.
Name them exactly after the field group key you want the template to be
used for.

For example, the template for the field group created using
`parent::__construct('bookfieldgroup', 'Book', array(...))` will be
loaded from `templates/fieldgroups/bookfieldgroup.php`

##### Fields

For rendering single fields, name them exactly after the field field
key. You might have different `title` fields in different field groups.
If you create a file `templates/fields/title.php` it will be rendered
regardless of which fieldgroup the `title` field is in.

However if you want to use a different template for the `title` field in
the `bookfieldgroup`, you'd create a file:
`templates/fields/bookfieldgroup.title.php`

##### Layouts

Create templates for layouts in the `templates/layouts` folder. Name
them exactly after the layout key you want the template to be used for.

For example, the template for the layout created using
`parent::__construct('paragraphlayout', 'Paragraph', array(...))` will
be loaded from `templates/layouts/paragraphlayout.php`

As with fields, your paragraph layout might be added to different
flexible content fields in different field groups. You can override
loading the default `templates/layouts/paragraphlayout.php` by creating
a file
`templates/layouts/[fieldgroupkey].[flexiblecontentfield].paragraphlayout.php`.

#### Template Context Data

##### ACF Data

Within templates, use `$this->[fieldkey]` to acces loaded data. Use
`var_dump($this->data)` to see what data is available.

##### Post Id

The current post id is available by using `$this->id`

##### Manipulating ACF Data Before Template Rendering

Override the `process` function on ACF Components to manipulate or add
additional data to a template.

    public function process($data, $id = null){
        return $data;
    }

ACF ContentPress I18n
=====================

ACF ContentPress I18n is a WordPress plugin which brings
Internationalization to ACFCP and ACFCP Frontend. Included is a custom
routing including translatable slugs for posts, post types and
taxonomies. A navigation (back and frontend) with translatable labels is
also provided.

Features
--------

-   Manage languages in the WP Admin
-   Fields marked for translated are automatically duplicated for every
    language
-   Translate post slugs
-   Translate post types
-   Translate taxonomies and terms
-   Translate navigation labels
-   Routing to handle translated permalinks

Installation
------------

Use Composer install:
`composer require premiummedia/acfcontentpressi18n`

Usage
-----

### Manage Languages

After plugin activation, navigate to the ACFCP I18n settings page in the
WordPress admin area. Select "Activate I18n" and add however many
languages you need.

### Mark Fields For Translation

Set the second parameter of the
`addField(Field $field, bool $translate)` function to `true` to mark a
field for translation.

### Translate Post & Page Slugs / Taxonomy & Terms / Navigation Labels

After plugin activation, language specific fields will be available in
the WP admin edit screen.

#### Custom Post Type & Taxonomy Names

Add your CPT and Taxonomy translations to your CPT / Tax registration in
`contents/register.php`. See the "Registering ACF Components, Custom
Post Types and Taxonomies" section in the ACFCP Manual for an example.

### Home Page Routing

In the WP admin, select "Settings &gt; Reading" and choose "A static
page" under "Front page displays". Then choose a page from the "Front
page:" dropdown.
