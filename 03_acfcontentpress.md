# ACF ContentPress

ACF ContentPress is a WordPress plugin based on the ACF (Advanced Custom Fields) WordPress plugin.

## Features

- Create ACF Field Groups using PHP
- Register Custom Post Types using PHP
- Register Taxonomies using PHP

- Easily create your own custom fields by extending acfcontentpress\\core\\Field
- Replace the standard WordPress post title by defining your own title field

## Installation

Use Composer install:
`composer require premiummedia/acfcontentpress`

## Usage

By default, ACFCP will look for a `contents` directory in in your active theme folder. Inside this directory you may add folders to separate your field group, field and layout definitions. Register your defined classes in the `contents/register.php` file.

Go and have a look at the `example` folder in the acfcontentpress repository.

### Registering ACF Components, Custom Post Types and Taxonomies

In the `contents/register.php` file, you register defined ACF field groups by adding its lowercase classname as a string to the $fieldGroups array.

```
class Contents{
    public static $fieldGroups = array(
        // ---
        'bookfieldgroup',
        ...
        // ---
    );

    public static $customPostTypes = array(
        'book' => array(
            'args' => array(
                'label' => 'Buch',
                'public' => true
            ),
            // 'names' for ACFCP I18n use
            /*'names' => array(
                'de' => 'buch',
                'fr' => 'livre',
                'en' => 'book',
                'it' => '?',
            )*/
        ),
        'anotherCPT' => array(
            ...
        )
    );

    public static $taxonomies = array(
        'tax1' => array(
            'object_type' => 'book',
            'args' => array(),
            // 'names' for ACFCP I18n use
            /*'names' => array(
                'de' => 'tax1_de',
                'fr' => 'tax1_fr',
                'en' => 'tax1_en',
                'it' => 'tax1_it'
            )*/
        ),
        'tax2' => array(
            ...
        )
    );
}

```

#### Custom Post Types

Create custom post types by adding them to the $customPostTypes array(). The parameter syntax is the same as the standard WordPress `register_post_type` function. Add the cpt name as array key and an array() with an `args` key as value.

#### Taxonomies

Create taxonomies by adding them to the $taxonomies array(). The parameter syntax is the same as the standard WordPress `register_taxonomy` function. Add the taxonomy name as array key and an array() with an `object_type` and `args` key as value.

### Autoloading

While you need to `use` ACF ContentPress classes, field groups, fields and layouts you create within `contents/[fieldgroups|fields|layouts]` are autoloaded in the global namespace.

### Code Pointers

Field Groups, Fields and Layouts all share the same base class and the same constructor, which is always
`($key, $label = '', $settings = array())`

The key is an identifying slug, which must be unique within an field collection. More on field collections later.
The label is a human readable version of the key. If left blank, an uppercased version of the key is used.
The settings array is an array of settings for this specific ACF component. Different fields, layouts and groups have different settings. Have a look at the class you're using or extending to see possible settings.

### Replacing Standard WordPress Title

Add array('replacesTitle' => true) to a (preferrably) TextField\'s settings to use this field\'s value to replace the WordPress post_title.

### Field Collections

Field collections are basically an array of field objects. For objects which have a field collection, you can add fields by calling `addField(Field $field, bool $translate)`.
Components with a field collection:
- Field groups
- Layouts
- Repeater fields

- Flexible content field

The flexible content field is a special case, because instead of adding fields to it via `addField()`, you use the same function to add pre-defined layouts: `addField(Layout $layout)`

### Creating Field Groups & Fields

Field groups can be defined by creating a class which extends `acfcontentpress\core\FieldGroup`. Call the parent constructor and add fields within the class constructor:

```
use acfcontentpress\core\FieldGroup;
use acfcontentpress\contrib\fields\TextField;

class BookFieldGroup extends FieldGroup{

    public function __construct(){

        $key = 'bookfieldgroup';
        $label = 'Book';
        $settings = array(
            'location' => array(
                array(
                    array(
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'book'
                    )
                )
            )
        )

        parent::__construct($key, $label, $settings);

        $this->addField(
            new TextField(
                'isbn',
                'ISBN Number',
                array(
                    'required' => true,
                    'maxlength' => 13
                )
            )
        );
    }

}

```

#### Adding Repeater Fields

Within field group or layout constructor:

```
$repeater = $this->addField(
    new RepeaterField(
        'slider',
        'Slider',
        array(
            'button_label' => 'New Slide',
            'min' => 2,
            'max' => 10
        )
    )
);

$repeater->addField(
    new ImageField(
        'image',
        'Image',
        array(
            'required' => true
        )
    )
);

$repeater->addField(
    new TextareaField(
        'caption'
    )
);
```

When calling `$this->addField`, the added field is returned. You can use that to add fields to it by saving it in a variable and calling `addField` on it.

#### Adding Flexible Content Fields

Flexible content fields have layouts added to them, which themselves have a field collection. To get started, we first have to create one or more layouts.

##### Layouts

Layouts are defined by extending `acfcontentpress\core\Layout` and look pretty much the same as a field group.
Note that there is no need for a location setting, because we will be adding the layout to a flexible field directly by calling `$flexibeContentField->addField(Layout $layout)`.
This allows you to easily use your layout in multiple flexible content fields at the same time.

```
use acfcontentpress\core\Layout;
use acfcontentpress\contrib\fields\TextField;
use acfcontentpress\contrib\fields\TextareaField;

class ParagraphLayout extends Layout{
    function __construct(){
        parent::__construct('paragraphlayout', 'Paragraph');

        $this->addField(
            new TextField('title')
        );

        $this->addField(
            new TextareaField('text', 'Text')
        );
    }
}
```

##### Flexible Content Field

Note: _Using flexible content fields within layouts/flexible content fields is theoretically possible but you might want to look for an easier solution using multiple field groups and custom post types._

Within a field group constructor:

```
$fc = $this->addField(
    new FlexibleContentField(
        'multicontent',
        'Multicontent',
        array(
            'button_label' => 'Add Content'
        )
    )
);

$fc->addField(new ParagraphLayout());

```

### Fields

Fields are a basic key/value storage. You may create your own fields by extending `acfcontentpress\core\Field`.

But most of the time, using the bundled fields in `acfcontentpress\contrib\fields` will suffice.

Note the default settings, which, in addition to specific field settings, all fields share:

```
protected $globalSettings = array(
    'instructions' => '',
    'required' => 0,
    'conditional_logic' => 0,
    'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => ''
    ),
    'default_value' => '',
    'acf_type' => 'field',
    'replacesTitle' => 0
);

```

#### Bundled Fields

Here you will find a short overview, including the field-specific settings.

##### CheckboxField

A basic checkbox.

```
protected $defaultSettings = array(
    /* (array) Array of choices where the key ('red') is used as value and the value ('Red') is used as label */
    'choices' => array(
    ),
    /* (string) Specify the layout of the checkbox inputs. Defaults to 'vertical'.
    Choices of 'vertical' or 'horizontal' */
    'layout' => 0
);

```

##### DatepickerField

A date picker.

```
protected $defaultSettings = array(
    'display_format' => 'd.m.Y',
    'return_format' => 'd.m.Y',
    'first_day' => 1,
);

```

##### DatetimepickerField

A date and time picker.

```
protected $defaultSettings = array(
    'display_format' => 'd.m.Y H:i',
    'return_format' => 'd.m.Y H:i',
    'first_day' => 1,
    );

```

##### EmailField

A text field which includes email validation.

```
protected $defaultSettings = array(
    /* (string) Appears within the input. Defaults to '' */
    'placeholder' => '',
    /* (string) Appears before the input. Defaults to '' */
    'prepend' => '',
    /* (string) Appears after the input. Defaults to '' */
    'append' => ''
);
```

##### FileField

A file upload field.

```
protected $defaultSettings = array(
    /* (string) Specify the type of value returned by get_field(). Defaults to 'array'.
	Choices of 'array' (File Array), 'url' (File URL) or 'id' (File ID) */
	'return_format' => 'array',
	/* (string) Specify the file size shown when editing. Defaults to 'thumbnail'. */
	'preview_size' => 'thumbnail',
	/* (string) Restrict the file library. Defaults to 'all'.
	Choices of 'all' (All Files) or 'uploadedTo' (Uploaded to post) */
	'library' => 'all',
	/* (int) Specify the minimum filesize in MB required when uploading. Defaults to 0
	The unit may also be included. eg. '256KB' */
	'min_size' => 0,
	/* (int) Specify the maximum filesize in MB in px allowed when uploading. Defaults to 0
	The unit may also be included. eg. '256KB' */
	'max_size' => 0,
	/* (string) Comma separated list of file type extensions allowed when uploading. Defaults to '' */
	'mime_types' => ''
);

```

##### Flexible ContentField

The flexible content field acts as a blank canvas to which you can add an unlimited number of layouts with full control over the order. Each layout can contain 1 or more sub fields allowing you to create simple or complex flexible content layouts.

```
protected $defaultSettings = array(
    'button_label' => 'Add Row',
    'min' => '',
    'max' => '',
);

```

##### GalleryField

```
protected $defaultSettings = array(
    /* (int) Specify the minimum attachments required to be selected. Defaults to 0 */
    'min' => 0,
    /* (int) Specify the maximum attachments allowed to be selected. Defaults to 0 */
    'max' => 0,
    /* (string) Specify the image size shown when editing. Defaults to 'thumbnail'. */
    'preview_size' => 'thumbnail',
    /* (string) Restrict the image library. Defaults to 'all'.
    Choices of 'all' (All Images) or 'uploadedTo' (Uploaded to post) */
    'library' => 'all',
    /* (int) Specify the minimum width in px required when uploading. Defaults to 0 */
    'min_width' => 0,
    /* (int) Specify the minimum height in px required when uploading. Defaults to 0 */
    'min_height' => 0,
    /* (int) Specify the minimum filesize in MB required when uploading. Defaults to 0
    The unit may also be included. eg. '256KB' */
    'min_size' => 0,
    /* (int) Specify the maximum width in px allowed when uploading. Defaults to 0 */
    'max_width' => 0,
    /* (int) Specify the maximum height in px allowed when uploading. Defaults to 0 */
    'max_height' => 0,
    /* (int) Specify the maximum filesize in MB in px allowed when uploading. Defaults to 0
    The unit may also be included. eg. '256KB' */
    'max_size' => 0,
    /* (string) Comma separated list of file type extensions allowed when uploading. Defaults to '' */
    'mime_types' => ''
);

```

##### GeolocationField

Note: _Add your Google API key in the WP ACFCP settings._

```
protected $defaultSettings = array(
    'center_lat' => '46.942975',
    'center_lng' => '7.445129',
    'zoom' => '12'
);

```

##### ImageField

```
protected $defaultSettings = array(
    /* (string) Specify the type of value returned by get_field(). Defaults to 'array'.
    Choices of 'array' (Image Array), 'url' (Image URL) or 'id' (Image ID) */
    'return_format' => 'array',
    /* (string) Specify the image size shown when editing. Defaults to 'thumbnail'. */
    'preview_size' => 'thumbnail',
    /* (string) Restrict the image library. Defaults to 'all'.
    Choices of 'all' (All Images) or 'uploadedTo' (Uploaded to post) */
    'library' => 'all',
    /* (int) Specify the minimum width in px required when uploading. Defaults to 0 */
    'min_width' => 0,
    /* (int) Specify the minimum height in px required when uploading. Defaults to 0 */
    'min_height' => 0,
    /* (int) Specify the minimum filesize in MB required when uploading. Defaults to 0
    The unit may also be included. eg. '256KB' */
    'min_size' => 0,
    /* (int) Specify the maximum width in px allowed when uploading. Defaults to 0 */
    'max_width' => 0,
    /* (int) Specify the maximum height in px allowed when uploading. Defaults to 0 */
    'max_height' => 0,
    /* (int) Specify the maximum filesize in MB in px allowed when uploading. Defaults to 0
    The unit may also be included. eg. '256KB' */
    'max_size' => 0,
    /* (string) Comma separated list of file type extensions allowed when uploading. Defaults to '' */
    'mime_types' => ''
);

```

##### NumberField

```
protected $defaultSettings = array(
    /* (string) Appears within the input. Defaults to '' */
    'placeholder' => '',
    /* (string) Appears before the input. Defaults to '' */
    'prepend' => '',
    /* (string) Appears after the input. Defaults to '' */
    'append' => '',
    /* (int) Minimum number value. Defaults to '' */
    'min' => '',
    /* (int) Maximum number value. Defaults to '' */
    'max' => '',
    /* (int) Step size increments. Defaults to '' */
    'step' => ''
);

```

##### OembedField

```
protected $defaultSettings = array(
    /* (int) Specify the width of the oEmbed element. Can be overridden by CSS */
    'width' => '',
    /* (int) Specify the height of the oEmbed element. Can be overridden by CSS */
    'height' => ''
);

```

##### PageLinkField

```
protected $defaultSettings = array(
    /* (mixed) Specify an array of post types to filter the available choices. Defaults to '' */
    'post_type' => '',
    /* (mixed) Specify an array of taxonomies to filter the available choices. Defaults to '' */
    'taxonomy' => '',
    /* (bool) Allow a null (blank) value to be selected. Defaults to 0 */
    'allow_null' => 0,
    /* (bool) Allow mulitple choices to be selected. Defaults to 0 */
    'multiple' => 0
);

```

##### PasswordField

```
protected $defaultSettings = array(
    /* (string) Appears within the input. Defaults to '' */
    'placeholder' => '',
    /* (string) Appears before the input. Defaults to '' */
    'prepend' => '',
    /* (string) Appears after the input. Defaults to '' */
    'append' => ''
);

```

##### PostObjectField

```
protected $defaultSettings = array(
    /* (mixed) Specify an array of post types to filter the available choices. Defaults to '' */
    'post_type' => '',
    /* (mixed) Specify an array of taxonomies to filter the available choices. Defaults to '' */
    'taxonomy' => '',
    /* (bool) Allow a null (blank) value to be selected. Defaults to 0 */
    'allow_null' => 0,
    /* (bool) Allow mulitple choices to be selected. Defaults to 0 */
    'multiple' => 0,
    /* (string) Specify the type of value returned by get_field(). Defaults to 'object'.
    Choices of 'object' (Post object) or 'id' (Post ID) */
    'return_format' => 'object'
);

```

##### RadioField

```
protected $defaultSettings = array(
    /* (array) Array of choices where the key ('red') is used as value and the value ('Red') is used as label */
    'choices' => array(
    ),
    /* (bool) Allow a custom choice to be entered via a text input */
    'other_choice' => 0,
    /* (bool) Allow the custom value to be added to this field's choices. Defaults to 0.
    Will not work with PHP registered fields, only DB fields */
    'save_other_choice' => 0,
    /* (string) Specify the layout of the checkbox inputs. Defaults to 'vertical'.
    Choices of 'vertical' or 'horizontal' */
    'layout' => 0
);

```

##### RelationshipField

```
protected $defaultSettings = array(
    /* (mixed) Specify an array of post types to filter the available choices. Defaults to '' */
    'post_type' => '',
    /* (mixed) Specify an array of taxonomies to filter the available choices. Defaults to '' */
    'taxonomy' => '',
    /* (array) Specify the available filters used to search for posts.
    Choices of 'search' (Search input), 'post_type' (Post type select) and 'taxonomy' (Taxonomy select) */
    'filters' => array('search', 'post_type', 'taxonomy'),
    /* (array) Specify the visual elements for each post.
    Choices of 'featured_image' (Featured image icon) */
    'elements' => array(),
    /* (int) Specify the minimum posts required to be selected. Defaults to 0 */
    'min' => 0,
    /* (int) Specify the maximum posts allowed to be selected. Defaults to 0 */
    'max' => 0,
    /* (string) Specify the type of value returned by get_field(). Defaults to 'object'.
    Choices of 'object' (Post object) or 'id' (Post ID) */
    'return_format' => 'object'
);

```

##### RepeaterField

```
protected $defaultSettings = array(
    'button_label' => 'Add Row',
    'min' => '',
    'max' => '',
    'layout' => 'row',
    'collapsed' => ''
);

```

##### SelectField

```
protected $defaultSettings = array(
    /* (array) Array of choices where the key ('red') is used as value and the value ('Red') is used as label */
    'choices' => array(
    ),
    /* (bool) Allow a null (blank) value to be selected. Defaults to 0 */
    'allow_null' => 0,
    /* (bool) Allow mulitple choices to be selected. Defaults to 0 */
    'multiple' => 0,
    /* (bool) Use the select2 interfacte. Defaults to 0 */
    'ui' => 0,
    /* (bool) Load choices via AJAX. The ui setting must also be true for this to work. Defaults to 0 */
    'ajax' => 0,
    /* (string) Appears within the select2 input. Defaults to '' */
    'placeholder' => ''
);

```

##### TaxonomyField

```
protected $defaultSettings = array(
    /* (string) Specify the taxonomy to select terms from. Defaults to 'category' */
    'taxonomy' => '',
    /* (array) Specify the appearance of the taxonomy field. Defaults to 'checkbox'
    Choices of 'checkbox' (Checkbox inputs), 'multi_select' (Select field - multiple), 'radio' (Radio inputs) or 'select' (Select field) */
    'field_type' => 'checkbox',
    /* (bool) Allow a null (blank) value to be selected. Defaults to 0 */
    'allow_null' => 0,
    /* (bool) Allow selected terms to be saved as relatinoships to the post */
    'load_save_terms' 	=> 0,
    /* (string) Specify the type of value returned by get_field(). Defaults to 'id'.
    Choices of 'object' (Term object) or 'id' (Term ID) */
    'return_format'		=> 'id',
    /* (bool) Allow new terms to be added via a popup window */
    'add_term'			=> 1
);

```

##### TextareaField

```
protected $defaultSettings = array(
    /* (string) Appears within the input. Defaults to '' */
    'placeholder' => '',
    /* (string) Restricts the character limit. Defaults to '' */
    'maxlength' => '',
    /* (int) Restricts the number of rows and height. Defaults to '' */
    'rows' => '',
    /* (new_lines) Decides how to render new lines. Detauls to 'wpautop'.
    Choices of 'wpautop' (Automatically add paragraphs), 'br' (Automatically add <br>) or '' (No Formatting) */
    'new_lines' => '',
    /* (bool) Makes the input readonly. Defaults to 0 */
    'readonly' => 0,
    /* (bool) Makes the input disabled. Defaults to 0 */
    'disabled' => 0
);

```

##### TextField

```
protected $defaultSettings = array(
    'placeholder' => '',
    'prepend' => '',
    'append' => '',
    'maxlength' => '',
    'readonly' => 0,
    'disabled' => 0
);

```

##### TrueFalseField

```
protected $defaultSettings = array(
    /* (string) Text shown along side the checkbox */
    'message' => 0
);

```

##### UrlField

```
protected $defaultSettings = array(
    /* (string) Appears within the input. Defaults to '' */
    'placeholder' => ''
);

```

##### UserField

```
protected $defaultSettings = array(
    /* (array) Array of roles to limit the users available for selection */
    'role' => array(),
    /* (bool) Allow a null (blank) value to be selected. Defaults to 0 */
    'allow_null' => 0,
    /* (bool) Allow mulitple choices to be selected. Defaults to 0 */
    'multiple' => 0
);

```

##### WysiwygField

```
protected $defaultSettings = array(
    /* (string) Specify which tabs are available. Defaults to 'all'.
    Choices of 'all' (Visual & Text), 'visual' (Visual Only) or text (Text Only) */
    'tabs' => 'all',
    /* (string) Specify the editor's toolbar. Defaults to 'full'.
    Choices of 'full' (Full), 'basic' (Basic) or a custom toolbar (https://www.advancedcustomfields.com/resources/customize-the-wysiwyg-toolbars/) */
    'toolbar' => 'full',
    /* (bool) Show the media upload button. Defaults to 1 */
    'media_upload' => 1
);
```
