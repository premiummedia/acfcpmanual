# ACF ContentPress Frontend

AcF ContentPress Frontend is a WordPress plugin which provides an easy way to render ACF field groups and fields using templates.

NOTE:_This does not disturb the WordPress post/page template selection. It simply provides a `display()` method, which you can use wherever you like._

## Features

- display ACF components using `display()`
- split your code into templates specific to an ACF component
- process data of an ACF component before rendering
- get the WordPress post id in your templates using `$this->id`

## Installation

The ACF ContentPress plugin has to be installed and activated in order for ACFCP Frontend to work.

Use Composer install:
`composer require premiummedia/acfcontentpressfrontend`

## Usage

At the heart of ACFCP Frontend ist the `display` function. It loads the data associated with an ACF Component and renders a template using this data.

### Templates

By default ACFCP Frontend will look for a `templates` directory in your active theme folder. Inside this directory you may add folders to seperate your field group, field and layout templates.

Basic folder structure:

- templates
    - fieldgroups
        - ...
    - fields
        - ...
    - layouts
        - ...

#### Naming Templates

##### Fieldgroups

Create templates for field groups in the `templates/fieldgroups` folder. Name them exactly after the field group key you want the template to be used for.

For example, the template for the field group created using `parent::__construct('bookfieldgroup', 'Book', array(...))`
will be loaded from
`templates/fieldgroups/bookfieldgroup.php`

##### Fields

For rendering single fields, name them exactly after the field field key. You might have different `title` fields in different field groups. If you create a file `templates/fields/title.php` it will be rendered regardless of which fieldgroup the `title` field is in.

However if you want to use a different template for the `title` field in the `bookfieldgroup`, you\'d create a file:
`templates/fields/bookfieldgroup.title.php`

##### Layouts

Create templates for layouts in the `templates/layouts` folder. Name them exactly after the layout key you want the template to be used for.

For example, the template for the layout created using `parent::__construct('paragraphlayout', 'Paragraph', array(...))`
will be loaded from
`templates/layouts/paragraphlayout.php`

As with fields, your paragraph layout might be added to different flexible content fields in different field groups.
You can override loading the default `templates/layouts/paragraphlayout.php` by creating a file `templates/layouts/[fieldgroupkey].[flexiblecontentfield].paragraphlayout.php`.

#### Template Context Data

##### ACF Data

Within templates, use `$this->[fieldkey]` to acces loaded data. Use `var_dump($this->data)` to see what data is available.

##### Post Id

The current post id is available by using `$this->id`

##### Manipulating ACF Data Before Template Rendering

Override the `process` function on ACF Components to manipulate or add additional data to a template.

```
public function process($data, $id = null){
    return $data;
}
```
