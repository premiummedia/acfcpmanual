# ACF ContentPress I18n

ACF ContentPress I18n is a WordPress plugin which brings Internationalization to ACFCP and ACFCP Frontend. Included is a custom routing including translatable slugs for posts, post types and taxonomies. A navigation (back and frontend) with translatable labels is also provided.

## Features

- Manage languages in the WP Admin
- Fields marked for translated are automatically duplicated for every language
- Translate post slugs
- Translate post types
- Translate taxonomies and terms
- Translate navigation labels
- Routing to handle translated permalinks

## Installation

Use Composer install:
`composer require premiummedia/acfcontentpressi18n`

## Usage

### Manage Languages

After plugin activation, navigate to the ACFCP I18n settings page in the WordPress admin area. Select "Activate I18n" and add however many languages you need.

### Mark Fields For Translation

Set the second parameter of the `addField(Field $field, bool $translate)` function to `true` to mark a field for translation.

### Translate Post & Page Slugs / Taxonomy & Terms / Navigation Labels

After plugin activation, language specific  fields will be available in the WP admin edit screen.

#### Custom Post Type & Taxonomy Names

Add your CPT and Taxonomy translations to your CPT / Tax registration in `contents/register.php`. See the "Registering ACF Components, Custom Post Types and Taxonomies"  section in the ACFCP Manual for an example.

### Home Page Routing

In the WP admin, select "Settings > Reading" and choose "A static page" under "Front page displays". Then choose a page from the "Front page:" dropdown.
